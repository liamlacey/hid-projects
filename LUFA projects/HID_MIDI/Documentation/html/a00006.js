var a00006 =
[
    [ "CreateGenericHIDReport", "a00006.html#aef37445ff9a760af08f51ee7bc019efd", null ],
    [ "EVENT_USB_Device_ConfigurationChanged", "a00006.html#a953a275884b9e33629ff6323fca05252", null ],
    [ "EVENT_USB_Device_Connect", "a00006.html#aeff97648c9250a3d398bb0b74f040899", null ],
    [ "EVENT_USB_Device_ControlRequest", "a00006.html#a3f4ce439a74a152e3c8ffda5c7dd201a", null ],
    [ "EVENT_USB_Device_Disconnect", "a00006.html#ae88405d14d8d6dada9313520cb1501ec", null ],
    [ "HID_Task", "a00006.html#a2f9c85184db121fa698d6fae1029fb10", null ],
    [ "main", "a00006.html#a840291bc02cba5474a4cb46a9b9566fe", null ],
    [ "ProcessGenericHIDReport", "a00006.html#af809dbb2f3bb12c80f1f3908bbb21638", null ],
    [ "SetupHardware", "a00006.html#acb27e569c06a2797c5fd58ed39147448", null ]
];