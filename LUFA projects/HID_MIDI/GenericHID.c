/*
             LUFA Library
     Copyright (C) Dean Camera, 2012.

  dean [at] fourwalledcubicle [dot] com
           www.lufa-lib.org
*/

/*
  Copyright 2012  Dean Camera (dean [at] fourwalledcubicle [dot] com)

  Permission to use, copy, modify, distribute, and sell this
  software and its documentation for any purpose is hereby granted
  without fee, provided that the above copyright notice appear in
  all copies and that both that the copyright notice and this
  permission notice and warranty disclaimer appear in supporting
  documentation, and that the name of the author not be used in
  advertising or publicity pertaining to distribution of the
  software without specific, written prior permission.

  The author disclaim all warranties with regard to this
  software, including all implied warranties of merchantability
  and fitness.  In no event shall the author be liable for any
  special, indirect or consequential damages or any damages
  whatsoever resulting from loss of use, data or profits, whether
  in an action of contract, negligence or other tortious action,
  arising out of or in connection with the use or performance of
  this software.
*/

/** \file
 *
 *  Main source file for the GenericHID demo. This file contains the main tasks of the demo and
 *  is responsible for the initial application hardware configuration.
 */

#include "GenericHID.h"

#define F_CPU 16000000 //Already defined within the LUFA code
#define USART_BAUDRATE 31250 
#define BAUD_PRESCALE (((F_CPU / (USART_BAUDRATE * 16UL))) - 1) 

/** LUFA MIDI Class driver interface configuration and state information. This structure is
 *  passed to all MIDI Class driver functions, so that multiple instances of the same class
 *  within a device can be differentiated from one another.
 */


/** Main program entry point. This routine configures the hardware required by the application, then
 *  enters a loop to run the application tasks in sequence.
 */
int main(void)
{
    //set initial report values
    midiType = midiNote = midiVelOrAt = 0;
    padNumber = 0;
    padValue = 0;
    padVelocity = 110;
    reportCommandId = 0x00;
    reportCount = reportError = 0;
    
	SetupHardware();

	LEDs_SetAllLEDs(LEDMASK_USB_NOTREADY);
	sei();
    
    //==== HARDWARE MIDI INIT STUFF========
    UCSR1B |= (1 << RXEN1) | (1 << TXEN1);   // Turn on the transmission and reception circuitry 
    //UCSR1C = (1 << USBS1) | (1 << UCSZ10); // Don't need to do this
		
//		//set PD3 as output pulled high
//		DDRD |= (1 << PD3);
//		PORTD |= (1 << PD3);

    UBRR1H = (BAUD_PRESCALE >> 8); // Load upper 8-bits of the baud rate value into the high byte of the UBRR register 
    UBRR1L = BAUD_PRESCALE; // Load lower 8-bits of the baud rate value into the low byte of the UBRR register 

	for (;;)
	{
//        //Test - alert that a pad data report that has a command ID of 0x01 needs to be sent
//        reportCommandId = 0x01;
        
        //====HARDWARE MIDI STUFF======
        while ((UCSR1A & (1 << UDRE1)) == 0) {}; // Do nothing until UDR is ready for more data to be written to it 
        uart_put(0x90); 
        uart_put(0x3c); 
        uart_put(0x6e);
        _delay_ms(100);
        
        uart_put(0x80); 
        uart_put(0x3c); 
        uart_put(0x00);
        _delay_ms(100);
        
        //create and send report
		HID_Task();
        //MIDI_Task();
		USB_USBTask();
        
//        //Test - interate pad data values to emulate pad presses
//        padValue++;
//        
//        if (padValue > 511)
//        {
//            padNumber++;
//            padValue = 0;
//        }
//    
//        if (padNumber > 47)
//        {
//            padNumber = 0;
//        }
//        
//        _delay_ms(5);
        
	}
}

//========FUNCTION FOR SENDING HARDWARE MIDI MESSAGES===========
void uart_put(char s)
{ 
    while ( !( UCSR1A & (1<<UDRE1)) ); // Wait for empty transmit buffer 
    UDR1 = s; // Put data in the buffer which is then sent by the AVR 
}

/** Configures the board hardware and chip peripherals for the demo's functionality. */
void SetupHardware(void)
{
	/* Disable watchdog if enabled by bootloader/fuses */
	MCUSR &= ~(1 << WDRF);
	wdt_disable();

	/* Disable clock division */
	clock_prescale_set(clock_div_1);

	/* Hardware Initialization */
	LEDs_Init();
	USB_Init();
}

/** Event handler for the USB_Connect event. This indicates that the device is enumerating via the status LEDs and
 *  starts the library USB task to begin the enumeration and USB management process.
 */
void EVENT_USB_Device_Connect(void)
{
	/* Indicate USB enumerating */
	LEDs_SetAllLEDs(LEDMASK_USB_ENUMERATING);
}

/** Event handler for the USB_Disconnect event. This indicates that the device is no longer connected to a host via
 *  the status LEDs and stops the USB management task.
 */
void EVENT_USB_Device_Disconnect(void)
{
	/* Indicate USB not ready */
	LEDs_SetAllLEDs(LEDMASK_USB_NOTREADY);
}

/** Event handler for the USB_ConfigurationChanged event. This is fired when the host sets the current configuration
 *  of the USB device after enumeration, and configures the generic HID device endpoints.
 */
void EVENT_USB_Device_ConfigurationChanged(void)
{
	bool ConfigSuccess = true;

	/* Setup HID Report Endpoints */
	ConfigSuccess &= Endpoint_ConfigureEndpoint(GENERIC_IN_EPADDR, EP_TYPE_INTERRUPT, GENERIC_EPSIZE, 1);
	ConfigSuccess &= Endpoint_ConfigureEndpoint(GENERIC_OUT_EPADDR, EP_TYPE_INTERRUPT, GENERIC_EPSIZE, 1);
    /* Setup MIDI Data Endpoints */
	ConfigSuccess &= Endpoint_ConfigureEndpoint(MIDI_STREAM_IN_EPADDR, EP_TYPE_BULK, MIDI_STREAM_EPSIZE, 1);
	ConfigSuccess &= Endpoint_ConfigureEndpoint(MIDI_STREAM_OUT_EPADDR, EP_TYPE_BULK, MIDI_STREAM_EPSIZE, 1);

	/* Indicate endpoint configuration success or failure */
	LEDs_SetAllLEDs(ConfigSuccess ? LEDMASK_USB_READY : LEDMASK_USB_ERROR);
}

/** Event handler for the USB_ControlRequest event. This is used to catch and process control requests sent to
 *  the device from the USB host before passing along unhandled control requests to the library for processing
 *  internally.
 */
void EVENT_USB_Device_ControlRequest(void)
{
	/* Handle HID Class specific requests */
	switch (USB_ControlRequest.bRequest)
	{
		case HID_REQ_GetReport:
			if (USB_ControlRequest.bmRequestType == (REQDIR_DEVICETOHOST | REQTYPE_CLASS | REQREC_INTERFACE))
			{
				uint8_t GenericData[GENERIC_REPORT_SIZE];
				CreateGenericHIDReport(GenericData);

				Endpoint_ClearSETUP();

				/* Write the report data to the control endpoint */
				Endpoint_Write_Control_Stream_LE(&GenericData, sizeof(GenericData));
				Endpoint_ClearOUT();
			}

			break;
		case HID_REQ_SetReport:
			if (USB_ControlRequest.bmRequestType == (REQDIR_HOSTTODEVICE | REQTYPE_CLASS | REQREC_INTERFACE))
			{
				uint8_t GenericData[GENERIC_REPORT_SIZE];

				Endpoint_ClearSETUP();

				/* Read the report data from the control endpoint */
				Endpoint_Read_Control_Stream_LE(&GenericData, sizeof(GenericData));
				Endpoint_ClearIN();

				ProcessGenericHIDReport(GenericData);
			}

			break;
	}
}

/** Function to process the last received report from the host.
 *
 *  \param[in] DataArray  Pointer to a buffer where the last received report has been stored
 */
void ProcessGenericHIDReport(uint8_t* DataArray)
{
	/*
		This is where you need to process reports sent from the host to the device. This
		function is called each time the host has sent a new report. DataArray is an array
		holding the report sent from the host.
	*/
    
    // DataArray[0] is equal to byte 1 (what we're using as a 'command ID') of the sent report.
    // As we're not using report IDs here, byte 0 of the sent report (0x00, default report ID) is not recieved here.
    // Is that statement corrent
    
    if (DataArray[0] == 0x06) //MIDI data OUT report command ID
    {
        //tell the CreateGenericHIDReport to create and send a reply report of this data
        reportCommandId = 0x07;
        
        //get midi status byte
        midiType = DataArray[1];
        //get note number from midi data byte 1
        midiNote = DataArray[2];
        //get velocity or aftertouch value from midi data byte 2
        midiVelOrAt = DataArray[3];
        
        
        //===get MIDI message bytes from the recieved report and create a MIDI message===
        
        uint8_t statusByte = DataArray[1];
        uint8_t dataByte1 = DataArray[2];
        uint8_t dataByte2 = DataArray[3];
        uint8_t midiCommand = statusByte & 0xf0;
        //uint8_t midiCommand = statusByte >> 4;
        
        /* Device must be connected and configured for the task to run */
        if (USB_DeviceState != DEVICE_STATE_Configured)
              return;
          
        Endpoint_SelectEndpoint(MIDI_STREAM_IN_EPADDR);
        
        if (statusByte)
        {
            MIDI_EventPacket_t MIDIEvent = (MIDI_EventPacket_t)
			{
				.Event       = MIDI_EVENT(0, midiCommand),
                
				.Data1       = statusByte,
				.Data2       = dataByte1,
				.Data3       = dataByte2,
			};
            
            /* Write the MIDI event packet to the endpoint */
            Endpoint_Write_Stream_LE(&MIDIEvent, sizeof(MIDIEvent), NULL);
            
            /* Send the data in the endpoint to the host */
            Endpoint_ClearIN();
        }
        
        while ((UCSR1A & (1 << UDRE1)) == 0) {};
        uart_put(statusByte); 
        uart_put(dataByte1); 
        uart_put(dataByte2);
        
        
        
        
        
    }
}

/** Function to create the next report to send back to the host at the next reporting interval.
 *
 *  \param[out] DataArray  Pointer to a buffer where the next report data should be stored
 */
void CreateGenericHIDReport(uint8_t* DataArray)
{
	/*
		This is where you need to create reports to be sent to the host from the device. This
		function is called each time the host is ready to accept a new report. DataArray is
		an array to hold the report to the host.
	*/
    
    // I think that as we're not using report IDs that DataArray variable doesn't need to include any kind of report ID (0x00).
    
    //set reportCommandId elsewhere to set which report you want to create.
    DataArray[0] = reportCommandId; // report Command ID

    //Pad data
    if (DataArray[0] == 1)
    {
        DataArray[1] = padNumber; //Pad number
        DataArray[2] = padValue & 0xFF; //first byte of pad pressure
        DataArray[3] = (padValue >> 8) & 0xFF; //second byte of pad pressure. is this where the 'little endian' formatting rule comes in?
        DataArray[4] = padVelocity; //Pad Velocity
        
        //DataArray[5] = DataArray[6] = DataArray[7] = 0;
    }
    
    //elite button data
    else if (DataArray[0] == 2)
    {
        DataArray[1] = 0; //button number
        DataArray[2] = 1; //button values
    }
    
    //elite dial data
    else if (DataArray[0] == 3)
    {
        DataArray[1] = 0; //dial number
        DataArray[2] = 1; //dial value
    }
    
    //MIDI reply test data
    else if (DataArray[0] == 7)
    {
        DataArray[1] = midiType;
        DataArray[2] = midiNote;
        DataArray[3] = midiVelOrAt;
        
        DataArray[4] = 0x00;
        DataArray[5] = reportCount;
        DataArray[6] = reportSize;
        DataArray[7] = reportError;
    }
    
    //reset command report ID?
    //reportCommandId = 0;
}

void HID_Task(void)
{
	/* Device must be connected and configured for the task to run */
	if (USB_DeviceState != DEVICE_STATE_Configured)
	  return;

	Endpoint_SelectEndpoint(GENERIC_OUT_EPADDR);
    
	/* Check to see if a packet has been sent from the host */
	if (Endpoint_IsOUTReceived())
	{
        reportSize = Endpoint_BytesInEndpoint();
        
        reportCount++;
        
		/* Check to see if the packet contains data */
		if (Endpoint_IsReadWriteAllowed())
		{
            //if a report size is 0, this if statement will not be entered
            
			/* Create a temporary buffer to hold the read in report from the host */
			uint8_t GenericData[GENERIC_REPORT_SIZE];
            for (int i = 0; i < GENERIC_REPORT_SIZE; i++)
                GenericData[i] = 0x00;

			/* Read Generic Report Data */
			reportError = Endpoint_Read_Stream_LE(&GenericData, sizeof(GenericData), NULL);

			/* Process Generic Report Data */
			ProcessGenericHIDReport(GenericData);
		}

		/* Finalize the stream transfer to send the last packet */
		Endpoint_ClearOUT();
	}

	Endpoint_SelectEndpoint(GENERIC_IN_EPADDR);

	/* Check to see if the host is ready to accept another packet */
	if (Endpoint_IsINReady())
	{
		/* Create a temporary buffer to hold the report to send to the host */
		uint8_t GenericData[GENERIC_REPORT_SIZE];

		/* Create Generic Report Data */
		CreateGenericHIDReport(GenericData);

		/* Write Generic Report Data */
		Endpoint_Write_Stream_LE(&GenericData, sizeof(GenericData), NULL);

		/* Finalize the stream transfer to send the last packet */
		Endpoint_ClearIN();
	}
}


/** Task to handle the generation of MIDI note change events in response to presses of the board joystick, and send them
 *  to the host.
 */
void MIDI_Task(void)
{
//	static uint8_t PrevJoystickStatus;
//    
//	/* Device must be connected and configured for the task to run */
//	if (USB_DeviceState != DEVICE_STATE_Configured)
//        return;
//    
//	Endpoint_SelectEndpoint(MIDI_STREAM_IN_EPADDR);
//    
//	if (Endpoint_IsINReady())
//	{
//		uint8_t MIDICommand = 0;
//		uint8_t MIDIPitch;
//        
//		uint8_t JoystickStatus  = Joystick_GetStatus();
//		uint8_t JoystickChanges = (JoystickStatus ^ PrevJoystickStatus);
//        
//		/* Get board button status - if pressed use channel 10 (percussion), otherwise use channel 1 */
//		uint8_t Channel = ((Buttons_GetStatus() & BUTTONS_BUTTON1) ? MIDI_CHANNEL(10) : MIDI_CHANNEL(1));
//        
//		if (JoystickChanges & JOY_LEFT)
//		{
//			MIDICommand = ((JoystickStatus & JOY_LEFT)? MIDI_COMMAND_NOTE_ON : MIDI_COMMAND_NOTE_OFF);
//			MIDIPitch   = 0x3C;
//		}
//        
//		if (JoystickChanges & JOY_UP)
//		{
//			MIDICommand = ((JoystickStatus & JOY_UP)? MIDI_COMMAND_NOTE_ON : MIDI_COMMAND_NOTE_OFF);
//			MIDIPitch   = 0x3D;
//		}
//        
//		if (JoystickChanges & JOY_RIGHT)
//		{
//			MIDICommand = ((JoystickStatus & JOY_RIGHT)? MIDI_COMMAND_NOTE_ON : MIDI_COMMAND_NOTE_OFF);
//			MIDIPitch   = 0x3E;
//		}
//        
//		if (JoystickChanges & JOY_DOWN)
//		{
//			MIDICommand = ((JoystickStatus & JOY_DOWN)? MIDI_COMMAND_NOTE_ON : MIDI_COMMAND_NOTE_OFF);
//			MIDIPitch   = 0x3F;
//		}
//        
//		if (JoystickChanges & JOY_PRESS)
//		{
//			MIDICommand = ((JoystickStatus & JOY_PRESS)? MIDI_COMMAND_NOTE_ON : MIDI_COMMAND_NOTE_OFF);
//			MIDIPitch   = 0x3B;
//		}
//        
//		/* Check if a MIDI command is to be sent */
//		if (MIDICommand)
//		{
//			MIDI_EventPacket_t MIDIEvent = (MIDI_EventPacket_t)
//            {
//                .Event       = MIDI_EVENT(0, MIDICommand),
//                
//                .Data1       = MIDICommand | Channel,
//                .Data2       = MIDIPitch,
//                .Data3       = MIDI_STANDARD_VELOCITY,
//            };
//            
//			/* Write the MIDI event packet to the endpoint */
//			Endpoint_Write_Stream_LE(&MIDIEvent, sizeof(MIDIEvent), NULL);
//            
//			/* Send the data in the endpoint to the host */
//			Endpoint_ClearIN();
//		}
//        
//		/* Save previous joystick value for next joystick change detection */
//		PrevJoystickStatus = JoystickStatus;
//	}
//    
//	/* Select the MIDI OUT stream */
//	Endpoint_SelectEndpoint(MIDI_STREAM_OUT_EPADDR);
//    
//	/* Check if a MIDI command has been received */
//	if (Endpoint_IsOUTReceived())
//	{
//		MIDI_EventPacket_t MIDIEvent;
//        
//		/* Read the MIDI event packet from the endpoint */
//		Endpoint_Read_Stream_LE(&MIDIEvent, sizeof(MIDIEvent), NULL);
//        
//		/* Check to see if the sent command is a note on message with a non-zero velocity */
//		if ((MIDIEvent.Event == MIDI_EVENT(0, MIDI_COMMAND_NOTE_ON)) && (MIDIEvent.Data3 > 0))
//		{
//			/* Change LEDs depending on the pitch of the sent note */
//			LEDs_SetAllLEDs(MIDIEvent.Data2 > 64 ? LEDS_LED1 : LEDS_LED2);
//		}
//		else
//		{
//			/* Turn off all LEDs in response to non Note On messages */
//			LEDs_SetAllLEDs(LEDS_NO_LEDS);
//		}
//        
//		/* If the endpoint is now empty, clear the bank */
//		if (!(Endpoint_BytesInEndpoint()))
//		{
//			/* Clear the endpoint ready for new packet */
//			Endpoint_ClearOUT();
//		}
//	}
}
 



