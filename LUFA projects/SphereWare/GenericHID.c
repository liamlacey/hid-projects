/*
             LUFA Library
     Copyright (C) Dean Camera, 2012.

  dean [at] fourwalledcubicle [dot] com
           www.lufa-lib.org
*/

/*
  Copyright 2012  Dean Camera (dean [at] fourwalledcubicle [dot] com)

  Permission to use, copy, modify, distribute, and sell this
  software and its documentation for any purpose is hereby granted
  without fee, provided that the above copyright notice appear in
  all copies and that both that the copyright notice and this
  permission notice and warranty disclaimer appear in supporting
  documentation, and that the name of the author not be used in
  advertising or publicity pertaining to distribution of the
  software without specific, written prior permission.

  The author disclaim all warranties with regard to this
  software, including all implied warranties of merchantability
  and fitness.  In no event shall the author be liable for any
  special, indirect or consequential damages or any damages
  whatsoever resulting from loss of use, data or profits, whether
  in an action of contract, negligence or other tortious action,
  arising out of or in connection with the use or performance of
  this software.
*/

#include "GenericHID.h"
#include "adc.h"
#include "digpot.h"
#include "mux.h"
#include "led.h"
#include "bootjump.h"

//#define F_CPU 16000000 //Already defined within the LUFA code.
#define USART_BAUDRATE 31250 
#define BAUD_PRESCALE (((F_CPU / (USART_BAUDRATE * 16UL))) - 1) 

#define LAST_PAD  1 
#define FIRST_PAD 0 

/** Main program entry point. This routine configures the hardware required by the application, then
 *  enters a loop to run the application tasks in sequence.
 */
uint8_t digpot_val[LAST_PAD];

int main(void)
{
    
    SetupHardware();
    sei();
    
    // === HARDWARE MIDI INIT CODE === 
    UCSR1B |= (1 << TXEN1);
    UBRR1H = (BAUD_PRESCALE >> 8);  
    UBRR1L = BAUD_PRESCALE;

    for (int pad = FIRST_PAD; pad < LAST_PAD; ++pad)
    {
        int16_t val;
        //even rows are on adc4 (==0) and odd rows are on adc5 (==1)
        uint8_t adc_number = (pad >> 3) % 2;

        MUX_Select(pad);
        DigPot_Write(0, 0x001);
        for (int i = 0; i < 100; ++i)
            val = ADC_Read(DIFF_0_X200, adc_number);

        for (;;)
        {
            if (val < 200)
            {
                digpot_val[pad] = DigPot_Read(0);
                break;
            }
            DigPot_Increment(0);
            val = ADC_Read(DIFF_0_X200, adc_number);
        }
    }

    for (;;)
    {

        if (!bit_is_set(PINE, PE2))
          BootJump_Jump_To_Bootloader();
        // === MAIN LOOP ===
        
        //read pads and send HID reports
        readPads();
        
        //read HID reports
        HID_Task();
        
        USB_USBTask();
        
    }
}


/** Function to process the last received report from the host.
 *
 *  \param[in] DataArray  Pointer to a buffer where the last received report has been stored
 */
void ProcessGenericHIDReport(uint8_t* DataArray)
{
    /*
     This is where you need to process reports sent from the host to the device. This
     function is called each time the host has sent a new report. DataArray is an array
     holding the report sent from the host.
     */
    
    // DataArray[0] is equal to byte 1 of the sent report (what we're using as a 'command ID') .
    // As we're not using report IDs here, byte 0 of the sent report (0x00, default report ID) is not recieved here.
    // Is that statement correct?
    
    if (DataArray[0] == 0x06) //received a MIDI message report
    {
        //to receive reports here they must be SENT in the following format:
        //byte 0 = report ID 0x00 (though this is ommited from DataArray)
        //byte 1 = report command ID 0x06 (DataArray[0])
        //byte 2 = MIDI status byte (DataArray[1])
        //byte 3 = MIDI data byte 1 (DataArray[2])
        //byte 4 = MIDI data byte 2 (DataArray[3])
        
        // === get MIDI message bytes from the recieved report and create a MIDI message ===
        
        uint8_t statusByte = DataArray[1];
        uint8_t dataByte1 = DataArray[2];
        uint8_t dataByte2 = DataArray[3];
        
        // === USB MIDI OUT ===
        
        /* Device must be connected and configured for the task to run */
        if (USB_DeviceState != DEVICE_STATE_Configured)
            return;
        
        Endpoint_SelectEndpoint(MIDI_STREAM_IN_EPADDR);
        
        if (statusByte)
        {
            uint8_t midiCommand = statusByte & 0xf0;
            
            MIDI_EventPacket_t MIDIEvent = (MIDI_EventPacket_t)
            {
                .Event       = MIDI_EVENT(0, midiCommand),
                .Data1       = statusByte,
                .Data2       = dataByte1,
                .Data3       = dataByte2,
            };
            
            /* Write the MIDI event packet to the endpoint */
            Endpoint_Write_Stream_LE(&MIDIEvent, sizeof(MIDIEvent), NULL);
            
            /* Send the data in the endpoint to the host */
            Endpoint_ClearIN();
        }
        
        // === HARDWARE MIDI OUT ===
        
        while ((UCSR1A & (1 << UDRE1)) == 0) {};
        uart_put(statusByte); 
        uart_put(dataByte1); 
        uart_put(dataByte2);

        //turn the LED blue
        int LEDChannels[NumLEDs][3];
        LEDChannels[0][0] = 0;
        LEDChannels[0][1] = 0;
        LEDChannels[0][2] = 1023;
        WriteLEDArray(LEDChannels);
 
    }
}


// === FUNCTION FOR READING PADS/ADC'S AND SENDING OUT THE DATA AS HID REPORTS
void readPads(void)
{
    int16_t pad_value[LAST_PAD];
    int16_t adc_0_val[LAST_PAD];

    MUX_Select(0);

    for(int pad = FIRST_PAD; pad < LAST_PAD; ++pad)
    {
        uint8_t adc_number = (pad >> 3) % 2;
        DigPot_Write(0, digpot_val[pad]);
        pad_value[pad] = ADC_Read(DIFF_0_X200, adc_number);

        if (pad == (LAST_PAD - 1))
            MUX_Select(0);
        else
            MUX_Select(pad + 1);

    }
    
    // === Send out pad data as HID reports ===
    
    Endpoint_SelectEndpoint(GENERIC_IN_EPADDR);
    
    /* Check to see if the host is ready to accept another packet */
    if (Endpoint_IsINReady())
    {
        /* Create a temporary buffer to hold the report to send to the host */
        uint8_t GenericData[(LAST_PAD*4)+1];
        
        //load the data into the buffer
        GenericData[0] = 0x01;
        
        for (int pad = FIRST_PAD; pad < LAST_PAD; ++pad)
        {
            GenericData[1+(pad*4)] = pad; // pad number
            GenericData[2+(pad*4)] = pad_value[pad] & 0xFF; //First byte of pad pressure
            GenericData[3+(pad*4)] = (pad_value[pad] >> 8) & 0xFF; //Second byte of pad pressure
            GenericData[4+(pad*4)] = adc_0_val[pad]; //adc value
        }
        
//      /* Create Generic Report Data */
//      CreateGenericHIDReport(GenericData);
        
        /* Write Generic Report Data */
        Endpoint_Write_Stream_LE(&GenericData, sizeof(GenericData), NULL);
        
        /* Finalize the stream transfer to send the last packet */
        Endpoint_ClearIN();
    }
}




/** Function to create the next report to send back to the host at the next reporting interval.
 *
 *  \param[out] DataArray  Pointer to a buffer where the next report data should be stored
 */
void CreateGenericHIDReport(uint8_t* DataArray)
{
//  /*
//     This is where you need to create reports to be sent to the host from the device. This
//     function is called each time the host is ready to accept a new report. DataArray is
//     an array to hold the report to the host.
//     */
//    
//    // I think that as we're not using report IDs that DataArray variable doesn't need to include any kind of report ID (0x00).
//    
//    //Pad data
//    if (DataArray[0] == 1)
//    {
//        for (int i = 0; i < 48; i++)
//        {
//            DataArray[1+(i*4)] = i; //Pad number
//            DataArray[2+(i*4)] = padValue[i] & 0xFF; //First byte of pad pressure
//            DataArray[3+(i*4)] = (padValue[i] >> 8) & 0xFF; //Second byte of pad pressure
//            DataArray[4+(i*4)] = padVelocity[i]; //Pad Velocity
//        }
//    }
//    
//    //elite button data
//    else if (DataArray[0] == 2)
//    {
//        DataArray[1] = 0; //button number
//        DataArray[2] = 1; //button values
//    }
//    
//    //elite dial data
//    else if (DataArray[0] == 3)
//    {
//        DataArray[1] = 0; //dial number
//        DataArray[2] = 1; //dial value
//    }
//    
}

void HID_Task(void)
{
    /* Device must be connected and configured for the task to run */
    if (USB_DeviceState != DEVICE_STATE_Configured)
        return;
    
    Endpoint_SelectEndpoint(GENERIC_OUT_EPADDR);
    
    /* Check to see if a packet has been sent from the host */
    if (Endpoint_IsOUTReceived())
    {
        /* Check to see if the packet contains data */
        if (Endpoint_IsReadWriteAllowed())
        {
            //if a report size is 0, this if statement will not be entered
            
            /* Create a temporary buffer to hold the read in report from the host */
            uint8_t GenericData[GENERIC_REPORT_SIZE];
            
            /* Read Generic Report Data */
            Endpoint_Read_Stream_LE(&GenericData, sizeof(GenericData), NULL);
            
            /* Process Generic Report Data */
            ProcessGenericHIDReport(GenericData);
        }
        
        /* Finalize the stream transfer to send the last packet */
        Endpoint_ClearOUT();
    }
    
//  Endpoint_SelectEndpoint(GENERIC_IN_EPADDR);
//    
//  /* Check to see if the host is ready to accept another packet */
//  if (Endpoint_IsINReady())
//  {
//      /* Create a temporary buffer to hold the report to send to the host */
//      uint8_t GenericData[GENERIC_REPORT_SIZE];
//        
//      /* Create Generic Report Data */
//      CreateGenericHIDReport(GenericData);
//        
//      /* Write Generic Report Data */
//      Endpoint_Write_Stream_LE(&GenericData, sizeof(GenericData), NULL);
//        
//      /* Finalize the stream transfer to send the last packet */
//      Endpoint_ClearIN();
//  }

}


// === FUNCTION FOR SENDING HARDWARE MIDI MESSAGES ===
void uart_put(char s)
{ 
    while ( !( UCSR1A & (1<<UDRE1)) );
    UDR1 = s;  
}

/** Configures the board hardware and chip peripherals for the demo's functionality. */
void SetupHardware(void)
{
    /* Disable watchdog if enabled by bootloader/fuses */
    MCUSR &= ~(1 << WDRF);
    wdt_disable();

    /* Disable clock division */
    clock_prescale_set(clock_div_1);

    /* Hardware Initialization */
    ADC_Init();
    DigPot_Init();
    MUX_Init();
    LED_Init();

    //LEDs_Init();
    USB_Init();
}

/** Event handler for the USB_Connect event. This indicates that the device is enumerating via the status LEDs and
 *  starts the library USB task to begin the enumeration and USB management process.
 */
void EVENT_USB_Device_Connect(void)
{
    /* Indicate USB enumerating */
    LEDs_SetAllLEDs(LEDMASK_USB_ENUMERATING);
}

/** Event handler for the USB_Disconnect event. This indicates that the device is no longer connected to a host via
 *  the status LEDs and stops the USB management task.
 */
void EVENT_USB_Device_Disconnect(void)
{
    /* Indicate USB not ready */
    LEDs_SetAllLEDs(LEDMASK_USB_NOTREADY);
}

/** Event handler for the USB_ConfigurationChanged event. This is fired when the host sets the current configuration
 *  of the USB device after enumeration, and configures the generic HID device endpoints.
 */
void EVENT_USB_Device_ConfigurationChanged(void)
{
    bool ConfigSuccess = true;

    /* Setup HID Report Endpoints */
    ConfigSuccess &= Endpoint_ConfigureEndpoint(GENERIC_IN_EPADDR, EP_TYPE_INTERRUPT, GENERIC_EPSIZE, 1);
    ConfigSuccess &= Endpoint_ConfigureEndpoint(GENERIC_OUT_EPADDR, EP_TYPE_INTERRUPT, GENERIC_EPSIZE, 1);
    /* Setup MIDI Data Endpoints */
    ConfigSuccess &= Endpoint_ConfigureEndpoint(MIDI_STREAM_IN_EPADDR, EP_TYPE_BULK, MIDI_STREAM_EPSIZE, 1);
    ConfigSuccess &= Endpoint_ConfigureEndpoint(MIDI_STREAM_OUT_EPADDR, EP_TYPE_BULK, MIDI_STREAM_EPSIZE, 1);

    /* Indicate endpoint configuration success or failure */
    LEDs_SetAllLEDs(ConfigSuccess ? LEDMASK_USB_READY : LEDMASK_USB_ERROR);
}

/** Event handler for the USB_ControlRequest event. This is used to catch and process control requests sent to
 *  the device from the USB host before passing along unhandled control requests to the library for processing
 *  internally.
 */
void EVENT_USB_Device_ControlRequest(void)
{
    /* Handle HID Class specific requests */
    switch (USB_ControlRequest.bRequest)
    {
        case HID_REQ_GetReport:
            if (USB_ControlRequest.bmRequestType == (REQDIR_DEVICETOHOST | REQTYPE_CLASS | REQREC_INTERFACE))
            {
                uint8_t GenericData[GENERIC_REPORT_SIZE];
                CreateGenericHIDReport(GenericData);

                Endpoint_ClearSETUP();

                /* Write the report data to the control endpoint */
                Endpoint_Write_Control_Stream_LE(&GenericData, sizeof(GenericData));
                Endpoint_ClearOUT();
            }

            break;
        case HID_REQ_SetReport:
            if (USB_ControlRequest.bmRequestType == (REQDIR_HOSTTODEVICE | REQTYPE_CLASS | REQREC_INTERFACE))
            {
                uint8_t GenericData[GENERIC_REPORT_SIZE];

                Endpoint_ClearSETUP();

                /* Read the report data from the control endpoint */
                Endpoint_Read_Control_Stream_LE(&GenericData, sizeof(GenericData));
                Endpoint_ClearIN();

                ProcessGenericHIDReport(GenericData);
            }

            break;
    }
}
