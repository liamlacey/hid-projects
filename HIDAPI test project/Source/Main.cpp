/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic startup code for a Juce application.

  ==============================================================================
*/

#include "../JuceLibraryCode/JuceHeader.h"
#include <stdio.h>
#include <wchar.h>
#include <string.h>
#include <stdlib.h>
#include "hidapi.h"

// Headers needed for sleeping.
#ifdef _WIN32
	#include <windows.h>
#else
	#include <unistd.h>
#endif

//==============================================================================
int main (int argc, char* argv[])
{

    // Do your application's initialisation code here..
        int res;
        unsigned char buf[5];
#define MAX_STR 255
        wchar_t wstr[MAX_STR];
        hid_device *handle;
        int i;
        
#ifdef WIN32
        UNREFERENCED_PARAMETER(argc);
        UNREFERENCED_PARAMETER(argv);
#endif
        
        struct hid_device_info *devs, *cur_dev;
        
        devs = hid_enumerate(0x0, 0x0);
        cur_dev = devs;	
        while (cur_dev) {
            printf("Device Found\n  type: %04hx %04hx\n  path: %s\n  serial_number: %ls", cur_dev->vendor_id, cur_dev->product_id, cur_dev->path, cur_dev->serial_number);
            printf("\n");
            printf("  Manufacturer: %ls\n", cur_dev->manufacturer_string);
            printf("  Product:      %ls\n", cur_dev->product_string);
            printf("  Release:      %hx\n", cur_dev->release_number);
            printf("  Interface:    %d\n",  cur_dev->interface_number);
            printf("\n");
            cur_dev = cur_dev->next;
        }
        hid_free_enumeration(devs);
        
        // Open the device using the VID, PID,
        // and optionally the Serial number.
        //handle = hid_open(0x03eb, 0x204f, NULL); // << LUFA demo HID device
        handle = hid_open(0x1d50, 0x6021, NULL); // << AlphaSphere HID device (once programmed)
        //handle = hid_open(0x1d50, 0x6041, NULL); // << AlphaSphere bootloader HID device 
        
        if (!handle) 
        {
            printf("unable to open device\n");

			#ifdef WIN32
            system("pause");
            #endif
            return 1;
        }

		// Set the hid_read() function to be non-blocking.
        hid_set_nonblocking(handle, 1);
        
		/*
        // Read the Manufacturer String
        wstr[0] = 0x0000;
        res = hid_get_manufacturer_string(handle, wstr, MAX_STR);
        if (res < 0)
            printf("Unable to read manufacturer string\n");
        printf("Manufacturer String: %ls\n", wstr);
        
        // Read the Product String
        wstr[0] = 0x0000;
        res = hid_get_product_string(handle, wstr, MAX_STR);
        if (res < 0)
            printf("Unable to read product string\n");
        printf("Product String: %ls\n", wstr);
        
        // Read the Serial Number String
        wstr[0] = 0x0000;
        res = hid_get_serial_number_string(handle, wstr, MAX_STR);
        if (res < 0)
            printf("Unable to read serial number string\n");
        printf("Serial Number String: (%d) %ls", wstr[0], wstr);
        printf("\n");
        
        // Read Indexed String 1
        wstr[0] = 0x0000;
        res = hid_get_indexed_string(handle, 1, wstr, MAX_STR);
        if (res < 0)
            printf("Unable to read indexed string 1\n");
        printf("Indexed String 1: %ls\n", wstr);
        */
        
    
    memset(buf,0,sizeof(buf));
    
    int counter = 0;
    for(;;)
    {
        //=== READ DEVICE ===
        
        res = hid_read(handle, buf, sizeof(buf));
        if (res == 0)
            //printf("no report...\n");
        if (res < 0)
            printf("Unable to read()\n");
        if (res > 0)
        {
            for (int i = 0; i < res; i++)
                printf("%i ", buf[i]);
            printf("\n");
            
        }
        
       // //=== SLEEP LOOP ===
       // 
       // //what should the following sleep value be
       // #ifdef WIN32
       // Sleep(500);
       // #else
       // usleep(500*1000);
       // #endif

        
        //=== WRITE TO DEVICE ===
        //note on midi message
		buf[0] = 0x00; //default report ID (first byte must always be a report ID!)
        buf[1] = 0x06; //We're using byte 1 as a 'command ID'. 0x06 signifies that this report is a midi message
		buf[2] = 0x90; //MIDI status byte
		buf[3] = counter; //MIDI data byte 1
        buf[4] = 0x07; //MIDI data byte 2
        res = hid_write(handle, buf, 6);
        if (res < 0)
            printf("Unable to write() (2)\n");
        
        //Without a delay here, the report below containing a note off message will be sent to the device,
        //the device recieves it, but the recieved report seems to be of an incomplete size (as displayed
        //from the 7th byte of the reply report back to the host (report size should be 5)
        //and THE ACTUALLY NOTE OFF MIDI MESSAGE ISN'T SENT BACK OUT AS A MIDI MESSAGE.
        //you can tell that the device is recieving the note-off report in some form as
        //the 6th byte of the reply report back to the host is a counter that interates everytime
        //a report is recieved within the devices HID_Task() function, however the report doesn't
        //seem to be processed.
        
        //Need to add a delay of about 100ms here for the note off message to be sent too.
        
//        #ifdef WIN32
//        Sleep(100);
//        #else
//        usleep(100*1000);
//        #endif
        
        //note-off midi message
		buf[0] = 0x00; //default report ID (first byte must always be a report ID!)
        buf[1] = 0x06; //We're using byte 1 as a 'command ID'. 0x06 signifies that this report is a midi message
		buf[2] = 0x80; //MIDI status byte
		buf[3] = counter; //MIDI data byte 1
        buf[4] = 0x00; //MIDI data byte 2
        res = hid_write(handle, buf, 6);
        if (res < 0)
            printf("Unable to write() (2)\n");
        
//        //poly aftertouch midi message
//		buf[0] = 0x00; //default report ID (first byte must always be a report ID!)
//        buf[1] = 0x06; //We're using byte 1 as a 'command ID'. 0x06 signifies that this report is a midi message
//		buf[2] = 0xA0; //MIDI status byte
//		buf[3] = counter; //MIDI data byte 1
//        buf[4] = 0x40; //MIDI data byte 2
//        res = hid_write(handle, buf, 6);
//        if (res < 0)
//            printf("Unable to write() (2)\n");
        
         res = 0;
		 memset(buf,0,sizeof(buf));
        
        counter++;
        if (counter > 127)
            counter = 0;
    }
        
        hid_close(handle);
        
        // Free static HIDAPI objects. 
        hid_exit();
        
#ifdef WIN32
        system("pause");
#endif


    return 0;
}
